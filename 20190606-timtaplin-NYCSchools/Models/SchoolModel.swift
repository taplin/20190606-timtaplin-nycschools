//
//  SchoolModel.swift
//  20190606-timtaplin-NYCSchools
//
//  Created by Tim Taplin on 6/6/19.
//  Copyright © 2019 AccessTelluride. All rights reserved.
//

import Foundation

struct SchoolModel: Codable {
    var dbn: String
    var schoolName: String?
    var boro: String?
    var overviewParagraph: String?
    var school10thSeats: String?
    var academicOpportunities1: String?
    var academicOpportunities2: String?
    var ellPrograms: String?
    var neighborhood: String?
    var buildingCode: String?
    var location: String?
    var phoneNumber: String?
    var faxNumber: String?
    var schoolEmail: String?
    var website: String?
    var subway: String?
    var bus: String?
    var grades2018: String?
    var finalGrades: String?
    var totalStudents: String?
    var extracurricularActivities: String?
    var schoolSports: String?
    var attendanceRate: String?
    var pctStuEnoughVariety: String?
    var pctStuSafe: String?
    var schoolAccessibilityDescription: String?
    var directions1: String?
    var requirement1: String?
    var requirement2: String?
    var requirement3: String?
    var requirement4: String?
    var requirement5: String?
    var offerRate: String?
    var program: String?
    var code: String?
    var interest: String?
    var method: String?
    var seats9ge: String?
    var grade9geFilledFlag: String?
    var grade9geApplicants: String?
    var seats9swd: String?
    var grade9swdFilledFlag: String?
    var grade9swdApplicants: String?
    var seats101: String?
    var admissionsPriority1: String?
    var admissionsPriority2: String?
    var admissionsPriority3: String?
    var grade9geApplicantsPerSeat: String?
    var grade9swdApplicantsPerSeat: String?
    var primaryAddressLine1: String?
    var city: String?
    var zip: String?
    var stateCode: String?
    var latitude: String?
    var longitude: String?
    var communityBoard: String?
    var councilDistrict: String?
    var censusTract: String?
    var bin: String?
    var bbl: String?
    var nta: String?
    var borough: String?
    
    enum CodingKeys: String, CodingKey{
        case dbn
        case schoolName = "school_name"
        case boro
        case overviewParagraph = "overview_paragraph"
        case school10thSeats = "school_10th_seats"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        case ellPrograms = "ell_programs"
        case neighborhood
        case buildingCode = "building_code"
        case location
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website
        case subway
        case bus
        case grades2018
        case finalGrades = "finalgrades"
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        case pctStuEnoughVariety = "pct_stu_enough_variety"
        case pctStuSafe = "pct_stu_safe"
        case schoolAccessibilityDescription = "school_accessibility_description"
        case directions1
        case requirement1 = "requirement1_1"
        case requirement2 = "requirement2_1"
        case requirement3 = "requirement3_1"
        case requirement4 = "requirement4_1"
        case requirement5 = "requirement5_1"
        case offerRate = "offer_rate1"
        case program = "program1"
        case code = "code1"
        case interest = "interest1"
        case method = "method1"
        case seats9ge = "seats9ge1"
        case grade9geFilledFlag = "grade9gefilledflag1"
        case grade9geApplicants = "grade9geapplicants1"
        case seats9swd = "seats9swd1"
        case grade9swdFilledFlag = "grade9swdfilledflag1"
        case grade9swdApplicants = "grade9swdapplicants1"
        case seats101
        case admissionsPriority1 = "admissionspriority11"
        case admissionsPriority2 = "admissionspriority21"
        case admissionsPriority3 = "admissionspriority31"
        case grade9geApplicantsPerSeat = "grade9geapplicantsperseat1"
        case grade9swdApplicantsPerSeat = "grade9swdapplicantsperseat1"
        case primaryAddressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case latitude
        case longitude
        case communityBoard = "community_board"
        case councilDistrict = "council_district"
        case censusTract = "census_tract"
        case bin
        case bbl
        case nta
        case borough
    }
}
