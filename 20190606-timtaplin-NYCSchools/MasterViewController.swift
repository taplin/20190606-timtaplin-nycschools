//
//  MasterViewController.swift
//  20190606-timtaplin-NYCSchools
//
//  Created by Tim Taplin on 6/6/19.
//  Copyright © 2019 AccessTelluride. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil

    let app: AppDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.leftBarButtonItem = editButtonItem

//        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
//        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        requestApiData()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

//    @objc
//    func insertNewObject(_ sender: Any) {
//        objects.insert(NSDate(), at: 0)
//        let indexPath = IndexPath(row: 0, section: 0)
//        tableView.insertRows(at: [indexPath], with: .automatic)
//    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = app.schoolList![indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return app.schoolList?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = app.schoolList![indexPath.row]
        cell.textLabel!.text = object.schoolName
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }

//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            objects.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: .fade)
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//        }
//    }

    func requestApiData() {
        
        var apiRequest = URLRequest(url: app.apiSchoolUrl!)
        apiRequest.httpMethod = "GET"
        apiRequest.timeoutInterval = 30
        
        apiRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = app.apiAccess.dataTask(with: apiRequest) { data, response, error in
            if let error = error {
                //                self.handleClientError(error)
                print("client error: \(error)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    //                    self.handleServerError(response)
                    print("server error: \(response)")
                    return
            }
            if let mimeType = httpResponse.mimeType, ( mimeType == "application/json"),
                let data = data {
                
                if let schoolData: [SchoolModel] = self.app.parseSchoolJson(with: data){
                    self.app.schoolList = schoolData
                    self.app.filteredSchoolList = schoolData
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                }
                
                print("json retrieval success")
                
            }
            
        }
        task.resume()
        var apiSATRequest = URLRequest(url: app.apiSATUrl!)
        apiSATRequest.httpMethod = "GET"
        apiSATRequest.timeoutInterval = 30
        
        apiSATRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let satTask = app.apiAccess.dataTask(with: apiSATRequest) { data, response, error in
            if let error = error {
                //                self.handleClientError(error)
                print("client error: \(error)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    //                    self.handleServerError(response)
                    print("server error: \(response)")
                    return
            }
            if let mimeType = httpResponse.mimeType, ( mimeType == "application/json"),
                let data = data {
                
                if let satData: [SATModel] = self.app.parseSATJson(with: data){
                    self.app.satList = satData
                    self.app.filteredSATList = satData
                    
                    
                }
                
                print("json retrieval success")
                
            }
            
        }
        satTask.resume()
        
        
        
    }
}

