//
//  DetailViewController.swift
//  20190606-timtaplin-NYCSchools
//
//  Created by Tim Taplin on 6/6/19.
//  Copyright © 2019 AccessTelluride. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolOverviewTextView: UITextView!
    @IBOutlet weak var satTakersValueLabel: UILabel!
    @IBOutlet weak var criticalReadingValueLabel: UILabel!
    @IBOutlet weak var mathAvgValueLabel: UILabel!
    @IBOutlet weak var writingAvgValueLabel: UILabel!
    
    var viewLoadedFlag = false
    let app: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            
            schoolNameLabel.text = detail.schoolName
            schoolOverviewTextView.text = detail.overviewParagraph
            if let sat = app.satList?.first(where: {$0.dbn == detail.dbn}) {
                satTakersValueLabel.text = sat.numOfSATTestTakers
                criticalReadingValueLabel.text = sat.satCriticalReadingAvgScore
                mathAvgValueLabel.text = sat.satMathAvgScore
                writingAvgValueLabel.text = sat.satWritingAvgScore
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewLoadedFlag = true
        // Do any additional setup after loading the view.
        configureView()
    }

    var detailItem: SchoolModel? {
        didSet {
            if viewLoadedFlag {
                // Update the view.
                configureView()
            }
        }
    }


}

