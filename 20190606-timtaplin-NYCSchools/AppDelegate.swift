//
//  AppDelegate.swift
//  20190606-timtaplin-NYCSchools
//
//  Created by Tim Taplin on 6/6/19.
//  Copyright © 2019 AccessTelluride. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, URLSessionDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    var apiSchoolUrl: URL?
    var apiSATUrl: URL?
    
    //data models
    var schoolList: [SchoolModel]?
    var filteredSchoolList: [SchoolModel]?
    var satList: [SATModel]?
    var filteredSATList: [SATModel]?
    
    public lazy var apiAccess: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.waitsForConnectivity = true
        return URLSession(configuration: configuration,
                          delegate: self, delegateQueue: nil)
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let splitViewController = window!.rootViewController as! UISplitViewController
        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
        navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
        splitViewController.delegate = self
        // got schools json url from https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2
        if let plistApiUrl = Bundle.main.object(forInfoDictionaryKey: "apiSchoolURL") as? String {
            apiSchoolUrl = URL(string: plistApiUrl)!
        } else {
            apiSchoolUrl = URL(string:"https://data.cityofnewyork.us/resource/s3k6-pzi2.json" )
        }
        // got sata json url from https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4
        if let plistApiUrl = Bundle.main.object(forInfoDictionaryKey: "apiSATURL") as? String {
            apiSATUrl = URL(string: plistApiUrl)!
        } else {
            apiSATUrl = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Split view

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
        if topAsDetailController.detailItem == nil {
            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
            return true
        }
        return false
    }

    func parseSchoolJson(with data: Data) -> [SchoolModel]?{
        
        do {
            
            let schoolsList: [SchoolModel] = try JSONDecoder().decode([SchoolModel].self, from: data)
            print("response decoded")
            return schoolsList
            //            NotificationCenter.default.post(name: .dataDownloadCompleted, object: nil)
            
        } catch {
            print("Parsing Failed \(error.localizedDescription)")
        }
        return nil
    }
    
    func parseSATJson(with data: Data) -> [SATModel]?{
        
        do {
            
            let satList: [SATModel] = try JSONDecoder().decode([SATModel].self, from: data)
            print("response decoded")
            return satList
            //            NotificationCenter.default.post(name: .dataDownloadCompleted, object: nil)
            
        } catch {
            print("Parsing Failed \(error.localizedDescription)")
        }
        return nil
    }
}

