//
//  AppDelegateTests.swift
//  20190606-timtaplin-NYCSchoolsTests
//
//  Created by Tim Taplin on 6/6/19.
//  Copyright © 2019 AccessTelluride. All rights reserved.
//

import XCTest
@testable import _0190606_timtaplin_NYCSchools

class AppDelegateTests: XCTestCase {
    let app: AppDelegate = UIApplication.shared.delegate as! AppDelegate

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testSchoolJsonParsing() {
        //Given
        let path = Bundle.main.path(forResource: "schoolData", ofType: "json")
    
        //When
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            if let schoolData = app.parseSchoolJson(with: data) {                 //Expect
                if let sampleSchool = schoolData.first(where: {$0.dbn == "02M260" }){
                    XCTAssertEqual(sampleSchool.schoolEmail, "admissions@theclintonschool.net")
                } else {
                    XCTFail(" retrieving sample school failed")
                }
            } else {
                XCTFail("Parsing Failed.")
            }
        } catch {
            // handle error
            XCTFail("Reading file Failed.")
        }
        
    }
    
    func testSATJsonParsing() {
        //Given
        let path = Bundle.main.path(forResource: "satData", ofType: "json")
        
        //When
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            if let satData = app.parseSATJson(with: data) { 
                //Expect
                if let sampleSAT = satData.first(where: {$0.dbn == "21K728" }){
                    XCTAssertEqual(sampleSAT.numOfSATTestTakers, "10")
                } else {
                    XCTFail(" retrieving sample school failed")
                }
            } else {
                XCTFail("Parsing Failed")
            }
        } catch {
            // handle error
            XCTFail("Reading file Failed")
        }
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
